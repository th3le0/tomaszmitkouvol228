﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wieza : MonoBehaviour
{
    int stopnie;
    public GameObject pocisk;
    GameObject pocisklecacy;
    Vector3 rotacja;
    float predkosc = 4f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Obrot");
    }

    // Update is called once per frame
    void Update()
    {
        //StartCoroutine("Obrot");
        //StopCoroutine("Obrot");
    }
    //void Obrot()
    //{
    //    stopnie = Random.Range(15, 45);
    //    Debug.Log("Stopnie" + stopnie);
    //    transform.rotation = Quaternion.Euler(Vector3.forward * stopnie);
    //}
    IEnumerator Obrot()
    {
        for(int i=1; i <13; i++)
        {
            stopnie = Random.Range(15, 45);
            Debug.Log("Stopnie" + stopnie);
            rotacja = Vector3.forward * stopnie;
            transform.eulerAngles += rotacja;
            Wystrzel();
            yield return new WaitForSeconds(.5f);
        }
    }
    void Wystrzel()
    {
        pocisklecacy = Instantiate(pocisk, Vector3.zero, Quaternion.Euler(rotacja));
        pocisklecacy.transform.Translate(Vector3.up * predkosc, Space.Self);
        //if(pocisklecacy.position)
    }
}
